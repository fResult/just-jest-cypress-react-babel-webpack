import React from 'react'
import CalculatorDisplay from 'calculator-display'
import { render } from 'calculator-test-utils'
import { light } from 'themes'

test('renders', () => {
  const { container, rerender } = render(
    <CalculatorDisplay value="0" />,
    { theme: light },
  )

  rerender(<CalculatorDisplay value="1" />)

  expect(container.firstChild).toMatchInlineSnapshot(`
    .emotion-0 {
      line-height: 130px;
      font-size: 6em;
      -webkit-flex: 1;
      -ms-flex: 1;
      flex: 1;
      position: relative;
      color: #1c191c;
      background: white;
    }

    <div
      class="emotion-0 emotion-1"
      id="container"
    >
      <div
        class="autoScalingText"
        data-testid="total"
        style="transform: scale(1,1);"
      >
        1
      </div>
    </div>
  `)
})
