import { fireEvent, render } from '@testing-library/react'
import React from 'react'

import Calculator from '../calculator'


test('renders', () => {
  const screen = render(<Calculator />)
  const clearButton = screen.getByText('AC')

  fireEvent.click(screen.getByText(/3/))
  expect(clearButton).toHaveTextContent('C')

  fireEvent.click(clearButton)
  // expect(clearButton.textContent).toBe('BC')
  expect(clearButton).toHaveTextContent('AC')
})
