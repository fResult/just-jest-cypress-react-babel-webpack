const path = require('path')

/** @type { import('eslint').Linter.BaseConfig } */
module.exports = {
  extends: ['kentcdodds', 'kentcdodds/import', 'kentcdodds/jest', 'kentcdodds/react'],
  overrides: [
    {
      files: ['**/src/**'],
      settings: { 'import/resolver': 'webpack' },
    },
    {
      files: ['**/__tests__/**'],
      settings: {
        'import/resolver': {
          jest: {
            jestConfigFile: path.join(__dirname, './jest.config.js')
          },
        },
      },
    },
  ],
}
