/** @type { import('@jest/types/build/Config').InitialOptions } */
module.exports = {
  ...require('./jest-common'),
  displayName: 'server',
  testEnvironment: 'jest-environment-node',
  testMatch: ['**/__server_tests__/**/*.js']
}
