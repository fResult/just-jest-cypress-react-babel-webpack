const path = require('path')

/** @type { import('@jest/types/build/Config').InitialOptions } */
module.exports = {
  rootDir: path.join(__dirname, '..'),
  displayName: 'lint',
  runner: 'jest-runner-eslint',
  testMatch: ['<rootDir>/**/*.js'],
}
