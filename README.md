<h1 align="center">
  Jest and Cypress with React, Babel, and Webpack
</h1>

<p align="center" style="font-size: 1.2rem;">
  See how to configure Jest and Cypress with React, Babel, and Webpack
</p>

<hr />

> Note: This project is intentionally over-engineered. The application itself is
> very simple, but the tooling around it is pretty complicated. The goal is to
> show what configuration would be like for a large real-world application
> without having all the extra complexities of a real-world application.

## License

This material is available for private, non-commercial use under the
[GPL version 3](http://www.gnu.org/licenses/gpl-3.0-standalone.html). If you
would like to use this material to conduct your own workshop, please contact me
at kent@doddsfamily.us

<h2 align="center">
  All Testing JS
</h2>

1. [Fundamentals of Testing in JavaScript](https://gitlab.com/fResult/just-testing-javascript)
2. [JavaScript Mocking Fundamentals](https://gitlab.com/fResult/just-mocking-js-fundamentals)
3. [Static Analysis Testing JavaScript Applications](https://gitlab.com/fResult/just-js-static-testing-tools)
4. [Use DOM Testing Library to test any JS framework](https://gitlab.com/fResult/just-dom-testing-library-with-anything)
5. [Configure Jest for Testing JavaScript Applications](https://gitlab.com/fResult/just-jest-cypress-react-babel-webpack) << You are here
6. [Test React Components with Jest and React Testing Library](https://gitlab.com/fResult/just-react-testing-library-course)
7. [Install, Configure, and Script Cypress for JavaScript Web Applications](#)
8. [Test Node.js Backends](#)
