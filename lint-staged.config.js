module.exports = {
  '**/*.+(js|json|less|css|html|ts|tsx|md)': [
    'prettier',
    'jest --findRelatedTests',
    'git add'
  ],
}
