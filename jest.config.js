/** @type { import('@jest/types/build/Config').InitialOptions } */
module.exports = {
  ...require('./test/jest-common'),
  collectCoverageFrom: ['**/src/**/*.js'],
  coverageThreshold: {
    global: {
      statements: 21,
      branches: 11,
      functions: 21,
      lines: 21,
    },
    './src/shared/utils.js': {
      statements: 100,
      branches: 80,
      functions: 100,
      lines: 100,
    },
  },
  projects: ['./test/jest.lint.js', './test/jest.client.js', './test/jest.server.js'],
}
